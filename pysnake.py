import pygame
import random
from game_constants import *
from game_elements import Snake, Food, PhoneWindow, Bonus, Malus#, Quirkles
import pdb

def draw_snake(screen, snake):
    screen.blit(snake.head.surf, snake.head.rect)
    if len(snake.body) > 0:
        for body_part in snake.body:
            screen.blit(body_part.surf, body_part.rect)

def is_colliding(obj):
    if obj.rect.colliderect(snake.head.rect):
        return True
    for body_part in snake.body:
        if obj.rect.colliderect(body_part.rect):
            return True

if __name__ == "__main__":

    pygame.mixer.init()
    pygame.mixer.pre_init(44100, -16, 2, 2048)
    pygame.init()
    score_font = pygame.font.Font(None, 25)
    clock = pygame.time.Clock()

    ### SCREEN
    screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    # set the pygame window name
    pygame.display.set_caption("is python a snake?")

    ### ALL SPRITE ELEMENTS
    all_characters = pygame.sprite.Group()

    ### NOKIA BG
    background_sprites = pygame.sprite.Group()
    phonewindow = PhoneWindow()
    background_sprites.add(phonewindow)

    ### SNAKE
    snake = Snake((10,10,255))
    all_characters.add(snake)
    score = 0


    score_file = 'high_scores.txt'
    if not os.path.exists(score_file):
        print("no high scores file, creating an empty one..")
        with open(score_file, 'w') as fp:
            fp.write("0")

    with open(score_file) as high_score_file:
        highest_score = int(high_score_file.readline())

    print(f"Your highest score was: {highest_score}")
    ### QUIRKLES
    #quirkles = pygame.sprite.Group()

    ### FOOD
    foods = pygame.sprite.Group()
    food = Food()
    foods.add(food)

    # BONUS / MALUS
    extras = pygame.sprite.Group()
    waiter_for_bonus = random.randint(BONUS_TIMER[0], BONUS_TIMER[1])
    waiter_for_malus = random.randint(MALUS_TIMER[0], MALUS_TIMER[1])
    bonus = Bonus()
    malus = Malus()
    remaining_life_b = 0
    remaining_life_m = 0

    running = True
    manually_add_food = False
    while running:
        for event in pygame.event.get():
            # QUIT
            if event.type == pygame.QUIT:
                running = False
            if event.type == KEYDOWN:
                if event.key == K_u:
                    manually_add_food = True

        pressed_keys = pygame.key.get_pressed()
        dead, ate_bonus, ate_malus, just_ate = snake.update(pressed_keys, food, manually_add_food, bonus, malus)
        if len(extras) > 0:
            remaining_life_b = bonus.update()
            remaining_life_m = malus.update()
            if (remaining_life_b < 1 or ate_bonus) and bonus in extras:
                if ate_bonus:
                    print(f"Great, +{remaining_life_b}")
                    score += remaining_life_b * SCORE_FACTOR
                bonus.kill()
                extras.remove(bonus)
            if (remaining_life_m < 1 or ate_malus) and malus in extras:
                if ate_malus:
                    print(f"Ouch! -{remaining_life_m}")
                    score -= remaining_life_m * SCORE_FACTOR
                malus.kill()
                extras.remove(malus)

        if dead:
            running=False
            print('== wrong move! you lost ==')

        # IF EATEN, SPAWN A NEW FOOD
        if just_ate or manually_add_food:
            if just_ate:
                score += SPEED
                waiter_for_bonus -= 1
                if waiter_for_bonus == 0:
                    if bonus in extras:
                        extras.remove(bonus)
                    bonus = Bonus()
                    while is_colliding(bonus):
                        print('respawning again, if this happens too often, use a better method for choosing position of the food')
                        bonus.kill()
                        bonus = Bonus()
                    extras.add(bonus)
                    waiter_for_bonus = random.randint(BONUS_TIMER[0], BONUS_TIMER[1])
                waiter_for_malus -= 1
                if waiter_for_malus == 0:
                    if malus in extras:
                        extras.remove(malus)
                    malus = Malus()
                    while is_colliding(malus):
                        print('respawning again, if this happens too often, use a better method for choosing position of the food')
                        malus.kill()
                        malus = Malus()
                    extras.add(malus)
                    waiter_for_malus = random.randint(MALUS_TIMER[0], MALUS_TIMER[1])
            food.kill()
            food = Food()
            while is_colliding(food):
                print('respawning again, if this happens too often, use a better method for choosing position of the food')
                food.kill()
                food = Food()
            foods.add(food)
            manually_add_food = False

        ## DRAWING ON THE SCREEN
        screen.fill(BACKGROUND_COLOR)
        for el in background_sprites:
            screen.blit(el.surf, el.rect)

        # blit transfer Surface onto another Surface (screen is a Surface)
        # Draw all sprites
        draw_snake(screen, snake)
        for food in foods:
            screen.blit(food.surf, food.rect)
        for extra in extras:
            screen.blit(extra.surf, extra.rect)
        if score > highest_score:
            highest_score = score
        score_el = score_font.render(f"score {score:05d}",
                                    True, (0,0,0))
        if bonus in extras:
            bonus_life = score_font.render(f"bonus: {remaining_life_b:04d}", True, (0, 0, 0))
            screen.blit(bonus_life, (605, 215))
        if malus in extras:
            malus_life = score_font.render(f"malus: {remaining_life_m:04d}", True, (0, 0, 0))
            screen.blit(malus_life, (485, 215))
        highest_score_el = score_font.render(f"record",
                                    True, (255, 255, 255))
        highest_score_el2 = score_font.render(f"{highest_score:05d}",
                                    True, (255, 255, 255))
        screen.blit(score_el, (175, 215))
        screen.blit(highest_score_el, (490, 65))
        screen.blit(highest_score_el2, (492, 85))
        pygame.display.flip()

        # Ensure program maintains a rate of X frames per second
        clock.tick(SPEED)

    print(f"\n-------------\nScore: {score:05d}\n-------------\n")
    print(f"Your highest score now is: {highest_score}")
    with open(score_file, 'w') as fff:
        fff.write(f"{highest_score}")

    pygame.mixer.music.stop()
    pygame.quit()
