# snake in python

![Screenshot of gameplay](gameplay/pysnake.gif)

A quick remake of the old but gold snake game with pygame.

### how to play
I assume you have python installed, simply install pygame (`pip install pygame`) if you haven't already and run

```
python3 pysnake.py
```

Set the speed in game_constants.py to make it faster (higher values) or slower (lower values).
It's not actually a speed, but rather the framerate of the update of the screen, but it quite comfortable this way.

It records your highest score by writing on a high_scores.txt file.
