import pygame
import random
import os
from game_constants import *
import numpy as np
import pdb
### DEFINITION OF THE NEEDED CLASSES
IMG_FOLDER = "assets/img"

# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
class Snake(pygame.sprite.Sprite):

    def add_body_part(self, x1, y1):
        body_part_sprite = pygame.sprite.Sprite()
        body_part_sprite.surf = pygame.Surface((CUBE_SIZE,CUBE_SIZE))
        body_part_sprite.rect = pygame.Rect(x1, y1, CUBE_SIZE, CUBE_SIZE)
        body_part_sprite.surf.fill((random.randint(0,80),random.randint(0,80),random.randint(120,255)))
        self.body.append(body_part_sprite)

    def remove_last_body_part(self):
        self.body.pop(0)

    def __init__(self, color):
        super(Snake, self).__init__()
        self.head = pygame.sprite.Sprite()
        self.head.surf = pygame.Surface((CUBE_SIZE,CUBE_SIZE))
        self.head.surf.fill(color)
        self.head.rect = self.head.surf.get_rect(
            center=(
                OFFSET_X + (CUBES_X/2)*CUBE_SIZE,
                OFFSET_Y + (CUBES_Y/2)*CUBE_SIZE,
            )
        )
        self.body = []
        self.pos = np.zeros(2) * (CUBES_X/2)*CUBE_SIZE
        self.dir = np.array((1,0))

    def update(self, pressed_keys, food, manually_add_food, bonus, malus):
        collided = False
        just_ate = False
        ate_bonus = False
        ate_malus = False

        self.add_body_part(self.head.rect.x, self.head.rect.y)
        ## updat with a formula
        # y = 1 - x**2 + x --> if x == 0 or 1: y = 1, if x == -1: y = -1
        # y = x**2 + x - 1 --> if x == 0 or -1: y = -1, if x == 1: y = 1
        # this two formulas allow to change and keep directions
        if pressed_keys[K_DOWN]:
            #print(f"pressed down, dir was : {self.dir}")
            self.dir = (0, 1 - (self.dir[1])**2 + self.dir[1])
            #print(f"pressed down, dir is: {self.dir}")
        elif pressed_keys[K_RIGHT]:
            #print(f"pressed right, dir was : {self.dir}")
            self.dir = (1 - (self.dir[0])**2 + self.dir[0], 0)
            #print(f"pressed right, dir is: {self.dir}")
        elif pressed_keys[K_UP]:
            #print(f"pressed up, dir was : {self.dir}")
            self.dir = (0, (self.dir[1])**2 + self.dir[1] - 1)
            #print(f"pressed up, dir is: {self.dir}")
        elif pressed_keys[K_LEFT]:
            #print(f"pressed left, dir was : {self.dir}")
            self.dir = ((self.dir[0])**2 + self.dir[0] - 1, 0)
            #print(f"pressed left, dir is: {self.dir}")
        shift = (self.dir[0]*CUBE_SIZE, self.dir[1]*CUBE_SIZE)
        self.pos += shift
        #print(f"pos: {self.pos}, dir: {self.dir}, shift: {shift}")
        if self.pos[0] >= OFFSET_X + SCREEN_WIDTH:
            self.pos[0] = int((self.pos[0] - SCREEN_WIDTH)/CUBE_SIZE) * CUBE_SIZE
        if self.pos[0] < OFFSET_X:
            self.pos[0] = int((SCREEN_WIDTH + self.pos[0])/CUBE_SIZE) * CUBE_SIZE
        if self.pos[1] >= OFFSET_Y + SCREEN_HEIGHT:
            self.pos[1] = int((self.pos[1] - SCREEN_HEIGHT)/CUBE_SIZE) * CUBE_SIZE
        if self.pos[1] < OFFSET_Y:
            self.pos[1] = int((SCREEN_HEIGHT + self.pos[1])/CUBE_SIZE) * CUBE_SIZE
        self.head.rect.update((self.pos[0], self.pos[1], CUBE_SIZE, CUBE_SIZE))

        for body_part in self.body:
            if self.head.rect.colliderect(body_part.rect):
                collided = True

        if food.rect.colliderect(self.head.rect):
            print('** yay -- eaten real food **')
            just_ate = True

        if bonus.rect.colliderect(self.head.rect):
            ate_bonus = True
        if malus.rect.colliderect(self.head.rect):
            ate_malus = True


        if manually_add_food:
            print('** mmm -- cheating.. **')

        ## CHECK IF IT ATE
        if not just_ate and not manually_add_food:
            self.remove_last_body_part()



        return collided, ate_bonus, ate_malus, just_ate
        #if len(self.body)> 0:
    #        print(f"body has {len(self.body)} elements")
    #        print(f"head is at: {self.head.rect}, body at: {self.body[0].rect}")

class Food(pygame.sprite.Sprite):

    def __init__(self, pos=None):
        super(Food, self).__init__()
        self.surf = pygame.Surface((CUBE_SIZE,CUBE_SIZE))
        food_design = random.randint(1,4)
        if food_design == 1:
            food_image = "food1.png"
        elif food_design == 2:
            food_image = "food2.png"
        elif food_design == 3:
            food_image = "food3.png"
        elif food_design == 4:
            food_image = "food4.png"
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, food_image)).convert_alpha()
        self.surf.set_colorkey(pygame.Color('#6abf81'), RLEACCEL)
        if not pos:
            pos = [CUBE_SIZE * random.randint(0, int(SCREEN_WIDTH/CUBE_SIZE-0.5)),
                  CUBE_SIZE * random.randint(0, int(SCREEN_HEIGHT/CUBE_SIZE-0.5))]
        self.rect = self.surf.get_rect(
            center=(
                OFFSET_X + OFFSET_X_FOOD + pos[0],
                OFFSET_Y + OFFSET_Y_FOOD + pos[1],
            )
        )

class PhoneWindow(pygame.sprite.Sprite):
    def __init__(self):
        super(PhoneWindow, self).__init__()
        img_name = "snake_bg_nassa3.png"
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, img_name)).convert()
        self.rect = self.surf.get_rect()

class Bonus(pygame.sprite.Sprite):
    def __init__(self, pos=None):
        super(Bonus, self).__init__()
        self.timer = random.randint(BONUS_LIFE[0], BONUS_LIFE[1])
        self.surf = pygame.Surface((CUBE_SIZE*2,CUBE_SIZE*2))
        bonus_image = "bonus.png"
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, bonus_image)).convert_alpha()
        self.surf.set_colorkey(pygame.Color('#6abf81'), RLEACCEL)
        if not pos:
            pos = [CUBE_SIZE * random.randint(0, int(SCREEN_WIDTH/CUBE_SIZE-2)),
                  CUBE_SIZE * random.randint(0, int(SCREEN_HEIGHT/CUBE_SIZE-2))]
        self.rect = self.surf.get_rect(
            center=(
                OFFSET_X + OFFSET_X_FOOD + pos[0],
                OFFSET_Y + OFFSET_Y_FOOD + pos[1],
            )
        )

    def update(self):
        self.timer -= 1
        return self.timer

class Malus(pygame.sprite.Sprite):
    def __init__(self, pos=None):
        super(Malus, self).__init__()
        self.timer = random.randint(MALUS_LIFE[0], MALUS_LIFE[1])
        self.surf = pygame.Surface((CUBE_SIZE*2,CUBE_SIZE*2))
        bonus_image = "malus.png"
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, bonus_image)).convert_alpha()
        self.surf.set_colorkey(pygame.Color('#6abf81'), RLEACCEL)
        if not pos:
            pos = [CUBE_SIZE * random.randint(0, int(SCREEN_WIDTH/CUBE_SIZE-2)),
                  CUBE_SIZE * random.randint(0, int(SCREEN_HEIGHT/CUBE_SIZE-2))]
        self.rect = self.surf.get_rect(
            center=(
                OFFSET_X + OFFSET_X_FOOD + pos[0],
                OFFSET_Y + OFFSET_Y_FOOD + pos[1],
            )
        )

    def update(self):
        self.timer -= 1
        return self.timer
